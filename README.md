# Eigen CI Testing.

Playground for Eigen CI testing with cross-compilation enabled.

To test a specific MR, set the following variables in the "Run Pipeline" section:
- `EIGEN_CI_GIT_URL`: git url for MR (e.g. `https://gitlab.com/cantonios/eigen.git`)
- `EIGEN_CI_GIT_BRANCH`: git branch to test (e.g. `my-mr-branch-name`)

Optional:
- `EIGEN_CI_BUILD_TARGET`: CMake build target to build for the `build` stage (e.g. `packetmath`)
  - If not set, will default to `buildtests` for CPU jobs, `buildtests_gpu` for GPU jobs.
- `EIGEN_CI_TEST_LABEL`: CTest label for tests to run in the `test` stage (e.g. `Official`)
  - If not set, will default to `Official` or `Unsupported` based on job type.
- `EIGEN_CI_TEST_REGEX`: regular expression for CTest targets to run in the `test` stage (e.g. `^packetmath`)
  - Takes precedence over `EIGEN_CI_TEST_LABEL`, useful for only testing a handful of tests across all platforms.
