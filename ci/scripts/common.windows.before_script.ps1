echo "Running ${CI_JOB_NAME}"

# Print configuration variables.
Get-Variable EIGEN* | Format-Table -Wrap
Get-Variable CMAKE* | Format-Table -Wrap

# Skip job if a job expression is provided and we don't match it.
if ("$EIGEN_CI_JOB_REGEX"  -And "$CI_JOB_NAME" -notmatch "$EIGEN_CI_JOB_REGEX" ) {
  echo "Job name ${CI_JOB_NAME} does not match regular expression ${EIGEN_CI_JOB_REGEX}, skipping test."
  exit
}

# Run a custom before-script command.
if ("${EIGEN_CI_BEFORE_SCRIPT}") { Invoke-Expression -Command "${EIGEN_CI_BEFORE_SCRIPT}" }

# ONLY FOR CI TESTING: clone into current directory
git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo
robocopy eigen_repo . /s /e > robocopy.log || echo 'copied files.'
