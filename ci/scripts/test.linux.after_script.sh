#!/bin/bash

set -x

# Skip job if a job expression is provided and we don't match it.
if [[ -n "$EIGEN_CI_JOB_REGEX" ]] && [[ ! "$CI_JOB_NAME" =~ $EIGEN_CI_JOB_REGEX ]]; then
  echo "Job name ${CI_JOB_NAME} does not match regular expression ${EIGEN_CI_JOB_REGEX}, skipping test."
  exit
fi

# Enter build directory.
rootdir=`pwd`
cd ${EIGEN_CI_BUILDDIR}

# Install xml processor.
apt-get update -y
apt-get install --no-install-recommends -y xsltproc

# Generate test results.
xsltproc ${rootdir}/ci/CTest2JUnit.xsl Testing/`head -n 1 < Testing/TAG`/Test.xml > "JUnitTestResults_$CI_JOB_ID.xml"

# Return to root directory.
cd ${rootdir}

set +x
