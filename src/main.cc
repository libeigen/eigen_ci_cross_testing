#include <iostream>
#include <string_view>

int main(int argc, char** argv) {
  if (argc > 1) {
    for (int i=1; i<argc; ++i) {
      std::cout << "Hello, " << std::string_view(argv[i]) << "!" << std::endl;  
    }
  } else {
    std::cout << "Hello, world!" << std::endl;
  }
  return 0;
}
